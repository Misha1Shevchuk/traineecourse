trigger AccountAddressTrigger on Account (before insert) {
    AccountAddressTriggerHandler.updateShippingPostalCode(Trigger.new);
}