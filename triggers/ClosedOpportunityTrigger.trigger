trigger ClosedOpportunityTrigger on Opportunity (after insert) {
    List<Task> tasks = ClosedOpportunityTriggerHandler.createTasks(Trigger.new);
    insert tasks;
}