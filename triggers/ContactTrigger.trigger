trigger ContactTrigger on Contact (after insert, after update, after delete) {
    if (Trigger.isAfter) {
        if (Trigger.isInsert || Trigger.isUpdate ) {
            ContactTriggerHandler.countContactsRelatedToAccount(Trigger.new);
        }
        if(Trigger.isDelete || Trigger.isUpdate){
            ContactTriggerHandler.countContactsRelatedToAccount(Trigger.old);
        }
    }
    //Todo: Trigger.isAfter Trigger.IsUpdate +
    // not never any logic in trigger!!!!! +
}